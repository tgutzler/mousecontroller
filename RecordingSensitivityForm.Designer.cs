﻿namespace MouseController
{
    partial class RecordingSensitivityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RecordingSensitivityForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelRecordingSensitivity = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.trackBarSensitivity = new System.Windows.Forms.TrackBar();
            this.cb_ShowEventsDroppedWarning = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSensitivity)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(8, 8);
            this.textBox1.Margin = new System.Windows.Forms.Padding(5);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(268, 83);
            this.textBox1.TabIndex = 3;
            this.textBox1.TabStop = false;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // labelRecordingSensitivity
            // 
            this.labelRecordingSensitivity.AutoSize = true;
            this.labelRecordingSensitivity.Location = new System.Drawing.Point(5, 115);
            this.labelRecordingSensitivity.Name = "labelRecordingSensitivity";
            this.labelRecordingSensitivity.Size = new System.Drawing.Size(197, 13);
            this.labelRecordingSensitivity.TabIndex = 4;
            this.labelRecordingSensitivity.Text = "Recording sensitivity: 42 events/second";
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(28, 182);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(181, 182);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // trackBarSensitivity
            // 
            this.trackBarSensitivity.LargeChange = 10;
            this.trackBarSensitivity.Location = new System.Drawing.Point(8, 138);
            this.trackBarSensitivity.Maximum = 100;
            this.trackBarSensitivity.Minimum = 1;
            this.trackBarSensitivity.Name = "trackBarSensitivity";
            this.trackBarSensitivity.Size = new System.Drawing.Size(268, 45);
            this.trackBarSensitivity.TabIndex = 6;
            this.trackBarSensitivity.TickFrequency = 5;
            this.trackBarSensitivity.Value = 42;
            this.trackBarSensitivity.Scroll += new System.EventHandler(this.trackBarSensitivity_Scroll);
            // 
            // cb_ShowEventsDroppedWarning
            // 
            this.cb_ShowEventsDroppedWarning.AutoSize = true;
            this.cb_ShowEventsDroppedWarning.Location = new System.Drawing.Point(8, 93);
            this.cb_ShowEventsDroppedWarning.Name = "cb_ShowEventsDroppedWarning";
            this.cb_ShowEventsDroppedWarning.Size = new System.Drawing.Size(93, 17);
            this.cb_ShowEventsDroppedWarning.TabIndex = 7;
            this.cb_ShowEventsDroppedWarning.Text = "Show warning";
            this.cb_ShowEventsDroppedWarning.UseVisualStyleBackColor = true;
            this.cb_ShowEventsDroppedWarning.CheckedChanged += new System.EventHandler(this.cb_ShowEventsDroppedWarning_CheckedChanged);
            // 
            // RecordingSensitivityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 217);
            this.Controls.Add(this.cb_ShowEventsDroppedWarning);
            this.Controls.Add(this.trackBarSensitivity);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelRecordingSensitivity);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RecordingSensitivityForm";
            this.Text = "Recording Sensitivity";
            this.Load += new System.EventHandler(this.RecordingSensitivityForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSensitivity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelRecordingSensitivity;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TrackBar trackBarSensitivity;
        private System.Windows.Forms.CheckBox cb_ShowEventsDroppedWarning;
    }
}