﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Windows.Forms;

namespace MouseController
{
    static class Program
    {
        public static string ApplicationDirectory
        {
            get => new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                if (Properties.Settings.Default.Version != Application.ProductVersion)
                {
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.Version = Application.ProductVersion;
                }

                Options opts = new Options(args);
                if (opts.ParseError != "")
                {
                    MessageBox.Show(opts.ParseError, "Error parsing options", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (opts.ShowHelp)
                {
                    MessageBox.Show(opts.Help, "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Application.Run(new MainForm(opts));
                    Trace.WriteLine("The End");
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
        }
    }
}
