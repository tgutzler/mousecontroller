﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MouseController
{
    internal static class ChkVersion
    {
        private static System.Version _latestVersion;
        private static readonly System.Version _myVersion = new System.Version(Application.ProductVersion);

        public static async void Check()
        {
            try
            {
                // Get version from sf
                var UpdateUri = new System.Uri("https://sourceforge.net/p/mousecontroller/code/HEAD/tree/trunk/MouseController.ver?format=raw");
                var client = new WebClient();
                string data = await client.DownloadStringTaskAsync(UpdateUri).ConfigureAwait(false);
                if (data != null)
                {
                    _latestVersion = new System.Version(data);
                }
                if (_myVersion.CompareTo(_latestVersion) < 0)
                {
                    Popup();
                }
            }
            catch
            {
                // Could not fetch online version
            }
        }

        private static void Popup()
        {
            UpdateAvailable UA = new UpdateAvailable()
            {
                Version = _latestVersion
            };
            UA.ShowDialog();
        }
    }
}
