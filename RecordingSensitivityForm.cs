﻿using System;
using System.Windows.Forms;

namespace MouseController
{
    public partial class RecordingSensitivityForm : Form
    {
        public uint Sensitivity { get; private set; }
        public bool ShowEventsDroppedWarning { get; private set; }

        public RecordingSensitivityForm(uint sensitivity, bool showEventsDroppedWarning)
        {
            InitializeComponent();
            Sensitivity = sensitivity;
            ShowEventsDroppedWarning = showEventsDroppedWarning;
        }

        private void RecordingSensitivityForm_Load(object sender, EventArgs e)
        {
            UpdateSensitivityLabel();
            try
            {
                trackBarSensitivity.Value = (int)Sensitivity;
                cb_ShowEventsDroppedWarning.Checked = ShowEventsDroppedWarning;
            }
            catch (Exception)
            {
                Sensitivity = (uint)trackBarSensitivity.Maximum;
                trackBarSensitivity.Value = trackBarSensitivity.Maximum;
            }
        }

        private void trackBarSensitivity_Scroll(object sender, EventArgs e)
        {
            Sensitivity = (uint)trackBarSensitivity.Value;
            UpdateSensitivityLabel();
        }

        private void UpdateSensitivityLabel()
        {
            labelRecordingSensitivity.Text =
                $"Recording Sensitivity: {Sensitivity} event{(Sensitivity == 1 ? "" : "s")}/second";
        }

        private void cb_ShowEventsDroppedWarning_CheckedChanged(object sender, EventArgs e)
        {
            ShowEventsDroppedWarning = cb_ShowEventsDroppedWarning.Checked;
        }
    }
}
